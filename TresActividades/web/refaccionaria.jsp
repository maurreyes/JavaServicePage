<%-- 
    Document   : refaccionaria
    Created on : 15/03/2018, 12:22:03 AM
    Author     : mareyes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
          <title>Parcial 1</title>       
        <meta charset="utf-8">
        <link rel="shortcut icon" href="ico.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Style.css">
        <!--<link rel="stylesheet" href="fontawesome-free-5.0.6/web-fonts-with-css/CSS/fontawesome.min.css"> -->
        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
                        
        <nav class="navbar navbar-default" role="navigation">
            <div class="navigation-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                            
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>
                <a class="navbar-brand" href="https://www.gitlab.com/Rezamar"><img id="logo" src="mareyes.png" id="logo"></a>
            </div>
            
            <div class="navbar-collapse collapse">
                    
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="NPrimos.jsp">Números primos</a></li>
                    <li><a href="Formulario.jsp">Formulario1</a></li>
                    <li><a href="Figu.jsp">Cálculo de Figuras</a></li>
                    <li><a href="Formulario2.jsp">Formulario2</a></li>
                    <li><a href="refaccionaria.jsp">Refaccionaria</a></li>
                </ul>
            </div> 
       </nav>
</ul>
        
 <form name="comprar" action="./Refaccionaria" method="post">
       <table>
		<tr>                                                                        
                <th>Producto</th>
		<th>Cantidad</th>		
		</tr>
		<tr><td>Bateria</td>                        			
		<td><input type="text" name="bateria" value="0"></td>		
		</tr>
		<tr><td>Aceite Roshfrans</td>                        			
                <td><input type="text" name="aceite" value="0"></td>
		</tr>
		<tr><td>Limpiaparabrisas</td>                        			
                <td><input type="text" name="limpiador" value="0"></td>
			
		</tr>
                <tr><td>Banda de tiempo</td>                       			
                <td><input type="text" name="banda" value="0"></td>			
		</tr>                
	</table>        
     <br>
    	<button class="botonRegistro">Aceptar</button>
   
 </form>
</body>
</html>
