<%-- 
    Document   : Formulario2
    Created on : 11/03/2018, 03:51:30 AM
    Author     : mareyes
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Iniciar Sesión</title><link rel="shortcut icon" href="ico.ico">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/Style.css">
        <!--<link rel="stylesheet" href="fontawesome-free-5.0.6/web-fonts-with-css/CSS/fontawesome.min.css"> -->
        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.min.js"></script>
                        
        <nav class="navbar navbar-default" role="navigation">
            <div class="navigation-header">                
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                            
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                    </button>  
                <a class="navbar-brand" href="https://www.gitlab.com/Rezamar"><img id="logo" src="mareyes.png" id="logo"></a>
            </div>
            
            <div class="navbar-collapse collapse">                
                    
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="index.jsp">Menu Principal</a></li>
                    <li><a href="Formulario2.jsp">Registrarse</a></li>
                    <!--<li><a href="Figu.jsp">Cálculo de Figuras</a></li>                   
                    <li><a href="IniciarSesion.jsp">Iniciar Sesion</a></li>
                    <li><a href="Formulario2.jsp">Registrarse</a></li>
                    <li><a href="#">Refaccionaria</a></li>-->
                </ul>
            </div>
        </nav>
    </head>
    <body>
        <h1>Iniciar Sesión</h1>
        <script src="js/main.js"></script>
        <form action="iniciar" method="post" id="forminicio"><br>
            <label>Nombre</label>
            <input type="text" name="usuario" id="txtusuario"/><br><br>
            <label>Contraseña</label>
            <input type="password" name="pass" id="txtpass"/><br><br>
            <input type="button" value="Iniciar Sesion" id="btniniciar"/><br>
        </form>
        <br>
         si no tienes cuenta perro<a href="Formulario2.jsp">Registrate</a>
    </body>
</htm>

