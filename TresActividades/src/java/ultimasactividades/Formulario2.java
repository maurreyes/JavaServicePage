/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultimasactividades;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mareyes
 */
@WebServlet(name = "AutenticacionServlet", urlPatterns = {"/autenticacionServlet"})
public class Formulario2 extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");               

        try (PrintWriter out = response.getWriter()) {
            String Nombre = request.getParameter("nombre");
            String Telefono = request.getParameter("telefono");
            String Edad = request.getParameter("edad");
            String Email = request.getParameter("email");
            String Pass = request.getParameter("passwd");
            
            String texto = "/home/mareyes/Documents/UNIVERSIDAD/7/PwebJava/1/reg.txt";
            
            String checale = "";
            if(Nombre != null && !Nombre.equalsIgnoreCase("") && Pass != null && !Pass.equalsIgnoreCase(""))
            {
                
            }
            else
            {
                checale = "Revisa los datos";
            }
                                    
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");          
            out.println("<head>");
            out.println("<title>Respuesta</title>");
            out.println("<meta charset=\"utf-8\">");
            out.println("<link rel=\"shortcut icon\" href='./ico.ico'\">");
            out.println("<link href='./css/Style.css' rel='stylesheet' type='text/css'>");                        
            out.println("<script src='./js/jquery-3.3.1.js'\"></script>");
            out.println("<script src='./js/bootstrap.min.js'\"></script>");
            out.println("<nav class=\"navbar navbar-default\" role=\"navigation\">");
            out.println("<div class=\"navigation-header\">                ");
            out.println("<button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("<span class=\"icon-bar\"></span>");
            out.println("</button>");
            out.println("<a class=\"navbar-brand\" href=\"https://www.gitlab.com/Rezamar\"><img id=\"logo\" src=\"mareyes.png\" id=\"logo\"></a>");
            out.println("</div>");
            out.println("<div class=\"navbar-collapse collapse\">");
            out.println("<ul class=\"nav navbar-nav navbar-left\">");
            out.println("<li><a href=\"NPrimos.jsp\">Números primos</a></li>");
            out.println("<li><a href=\"Formulario.jsp\">Formulario1</a></li>");
            out.println("<li><a href=\"Figu.jsp\">Cálculo de Figuras</a></li>");
            out.println("<li><a href=\"Formulario2.jsp\">Formulario2</a></li>");
            out.println("<li><a href=\"refaccionaria.jsp\">Refaccionaria</a></li>");
            out.println("</ul>");
            out.println("</div>");
            out.println("</nav>");            
            out.println("</head>");       
            out.println("<body>");            
            out.println("<ul id='button'>");            
            out.println("</ul>");           
        
            out.println("<h1>Datos de Usuario</h1>");
            out.println("<form class='formulario'>");
            out.println("<h2>Nombre: " + Nombre + "</h2>");
            out.println("<h2>Telefono: " + Telefono + "</h2>");            
            out.println("<h2>Email: " + Email + "</h2>");
            out.println("</form>");
                        
            out.println("<font color='red'>"+checale+"</font>");
            out.println("</body>");
            out.println("</html>");
            
            FileOutputStream fileOS = new java.io.FileOutputStream(texto, false);
            OutputStreamWriter writer = new java.io.OutputStreamWriter(fileOS,"UTF-8");
            BufferedWriter file = new java.io.BufferedWriter(writer);
            file.write("Nombre: " + Nombre);
            file.newLine();
            file.write("Telefono: " + Telefono);
            file.newLine();                        
            file.write("Email: " + Email);
            file.newLine();
            file.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}